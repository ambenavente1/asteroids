package cs151.arcade.asteroids;

import cs151.arcade.asteroids.io.ConfigReader;
import cs151.arcade.asteroids.main.Asteroids;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.ScalableGame;
import org.newdawn.slick.SlickException;

import java.util.HashMap;

/**
 * Entry point to the program.  Starts up an AppGameContainer and loads in
 * the Asteroids game into it.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class Start {

    /**
     * Entry point to the program
     *
     * @param args No command line arguments are needed
     */
    public static void main(String[] args) {
        try {
            HashMap<String, String> config = ConfigReader.getConfigValues();

            final int WINDOW_WIDTH = Integer.parseInt(config.get("WIDTH"));
            final int WINDOW_HEIGHT = (int)(WINDOW_WIDTH / 1.333333333333);

            Game game = new Asteroids("Asteroids");
            ScalableGame scalableGame = new ScalableGame(game,
                    Asteroids.GAME_WIDTH, Asteroids.GAME_HEIGHT);
            AppGameContainer app = new AppGameContainer(scalableGame);
            app.setDisplayMode(WINDOW_WIDTH,
                               WINDOW_HEIGHT,
                    Boolean.parseBoolean(config.get("FULLSCREEN")));
            app.setTargetFrameRate(60);
            app.setShowFPS(false);
            app.start();
        } catch (SlickException e) {
            System.out.println("**** Something went really wrong...");
            System.out.println(e.getMessage());
        }
    }

}
