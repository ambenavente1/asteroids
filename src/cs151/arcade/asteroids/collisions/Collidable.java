package cs151.arcade.asteroids.collisions;

/**
 * Describes an object that may collide with another in which some code
 * gets executed immediately after.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/26/14
 */
public interface Collidable {

    /**
     * This method is the method that should be called when a collision
     * happens between this object and the object described through the
     * arguments passed in as a parameter.
     *
     * @param e arguments for another object colliding with this one
     */
    public void onCollide(CollideEventArgs e);
}
