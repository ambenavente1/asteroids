package cs151.arcade.asteroids.collisions;

import cs151.arcade.asteroids.objects.GameObject;
import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.weapons.bullets.Bullet;
import org.newdawn.slick.geom.Vector2f;

/**
 * A container for components that may be required for when an object
 * collides with another.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/26/14
 */
public class CollideEventArgs {

    /**
     * The position of the object described by this class
     */
    private Vector2f pos;

    /**
     * The mass of the object
     */
    private float mass;

    /**
     * The velocity of the object
     */
    private Vector2f vel;

    /**
     * The angle at which this object is moving
     */
    private float movementAngle;

    /**
     * The amount of damage this object can cause to another
     */
    private float damage;

    /**
     * Creates a new object to store information about a GameObject
     *
     * @param other the game object to store information for
     */
    public CollideEventArgs(GameObject other) {
        this.pos = other.getPos();
        this.mass = other.getMass();
        this.vel = other.getVel();
        this.movementAngle = other.getMovementAngle();

        if (other instanceof Bullet) {
            damage = ((Bullet)other).getParentDamage();
        }

        if (other instanceof Ship) {
            damage = ((Ship)other).getMass() / 2;
        }
    }

    /**
     * Gets the position of the object described by this class
     *
     * @return the position of the object
     */
    public Vector2f getPos() {
        return pos.copy();
    }

    /**
     * Gets the mass of the object described by this class
     *
     * @return the mass of the object
     */
    public float getMass() {
        return mass;
    }

    /**
     * Gets the velocity of the object described by this class
     *
     * @return the velocity the object is travelling at
     */
    public Vector2f getVel() {
        return vel.copy();
    }

    /**
     * The angle that this object is moving
     *
     * @return the angle of movement the object is following
     */
    public float getMovementAngle() {
        return movementAngle;
    }

    /**
     * The amount of damage this object would do to another object
     *
     * @return damage value for this object
     */
    public float getDamage() {
        return damage;
    }
}
