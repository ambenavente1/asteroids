package cs151.arcade.asteroids.collisions;

import cs151.arcade.asteroids.objects.Asteroid;
import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.objects.managers.AsteroidManager;
import cs151.arcade.asteroids.particles.ParticleSystem;
import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.bullets.Bullet;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Handles collisions between asteroids, the ship,
 * and bullets of the ship's weapons.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/26/14
 */
public class CollisionManager implements Updatable {

    /**
     * The AsteroidManager in charge of updating asteroids
     */
    private AsteroidManager asteroidManager;

    /**
     * The ship controlled by the player
     */
    private Ship ship;

    /**
     * Creates a new collision manager by passing in references to objects
     * controlling asteroids, bullets, and the ship
     *
     * @param asteroidManager whatever is in charge of updating asteroids
     * @param ship            the ship with bounds and stuff
     */
    public CollisionManager(AsteroidManager asteroidManager,
                            Ship ship) {
        this.asteroidManager = asteroidManager;
        this.ship = ship;
    }

    /**
     * Checks if anything on the screen has collided with one another
     *
     * @param container the container in charge of handling the game loop
     * @param game      the state-based game in charge of the state system
     * @param delta     the amount of time passed from the last update
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {

        for (Asteroid a : asteroidManager) {
            for (Bullet bullet : ship.getWeapon().getBullets()) {
                if (a.getBounds().intersects(bullet.getBounds())) {
                    a.onCollide(new CollideEventArgs(bullet));
                    bullet.onCollide(new CollideEventArgs(a));
                    ship.addPoints((int) (a.getMass() * .25f));
                }
            }

            if (!ship.isInvincible()) {
                if (a.getBounds().intersects(ship.getBounds())) {
                    a.onCollide(new CollideEventArgs(ship));
                    ship.onCollide(new CollideEventArgs(a));

                    if (ship.isDead()) {
                        ParticleSystem.addExplosion(ship.centerPos());
                        ParticleSystem.addExplosion(ship.centerPos());
                        ParticleSystem.addExplosion(ship.centerPos());
                    }
                }
            }
        }
    }
}
