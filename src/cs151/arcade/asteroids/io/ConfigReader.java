package cs151.arcade.asteroids.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

/**
 * This class reads the configuration from the config file
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/30/14
 */
public class ConfigReader {

    /**
     * The name of the config file to be using
     */
    private static String PATH = System.getProperty("user.dir") +
            "/asteroids.config";

    /**
     * Returns a list of config values from the config file
     *
     * @return a hash map containing with a list of config values
     * identified by the name of the configuration
     */
    public static HashMap<String, String> getConfigValues() {
        HashMap<String, String> result = new HashMap<String, String>();

        File file = new File(PATH);
        if (!file.exists()) {
            try {
                file.createNewFile();

                PrintWriter fileWriter = null;
                try {
                    fileWriter = new PrintWriter(file);
                    fileWriter.println("WIDTH: 800");
                    fileWriter.println("FULLSCREEN: false");
                } catch (FileNotFoundException e) {
                    System.out.println("**** File could not be written to");
                    System.out.println(e.getMessage());
                } finally {
                    if (fileWriter != null) {
                        fileWriter.close();
                    }
                }
            } catch (IOException e) {
                System.out.println("**** Failed to create file");
                System.out.println(e.getMessage());
            }
        }


        Scanner fileReader = null;
        try {
            fileReader = new Scanner(file);
            String line;
            while (fileReader.hasNext()) {
                line = fileReader.nextLine();
                if (line.contains(":")) {
                    String[] config = line.split(":");
                    result.put(config[0].trim(), config[1].trim());
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("**** The file couldn't be read");
            System.out.println(e.getMessage());
        }

        return result;
    }
}
