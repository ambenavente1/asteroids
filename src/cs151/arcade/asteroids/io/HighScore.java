package cs151.arcade.asteroids.io;

/**
 * Represents an actual high score that resulted at the end of the a game.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/29/14
 */
public class HighScore implements Comparable<HighScore> {

    public static final String DELIMITER = "|";

    /**
     * The name of the player who created this high score
     */
    private String name;

    /**
     * The score of the player who created this high score
     */
    private int score;

    /**
     * Creates a new HighScore object by passing in the name of the person
     * who created the high score and the actual score itself.
     *
     * @param name  The name of the person owning the high score
     * @param score The actual score
     */
    public HighScore(String name, int score) {
        this.name = name;
        this.score = score;
    }

    /**
     * Gets the name of the person who achieved the score
     *
     * @return the name of the person who achieved the score
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the numerical score that the person achieved.  The score limit
     * is Integer.MAX_VALUE.
     *
     * @return the numerical score of the person
     */
    public int getScore() {
        return score;
    }


    /**
     * Gets if the score is higher, lower, or equal to the other score
     *
     * @param highScore the other high score to compare
     * @return if the return value is positive, this score is higher than
     * the other score, if the return value is negative,
     * this score is lower than the other score, and if the return value is
     * 0, the two values are equal
     */
    @Override
    public int compareTo(HighScore highScore) {
        return score - highScore.getScore();
    }

    /**
     * TODO: Swap the returns of toString() and formalString()
     * Returns a string representing the high score by separating the name
     * and score by a delimiter for use when reading the score
     *
     * @return a string representing the high score for use when splitting
     * by the delimiter
     */
    @Override
    public String toString() {
        return name + DELIMITER + score;
    }

    /**
     * Gets a string of the high score for use in displaying the high score
     * to the screen
     *
     * @return a more visually appealing representation of the high score
     */
    public String formalString() {
        return name + "  ----  " + score;
    }
}
