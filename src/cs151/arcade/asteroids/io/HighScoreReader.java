package cs151.arcade.asteroids.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * This class reads and returns a sorted list of high scores (highest -
 * lowest) from the scores file.  If a score file does not exist,
 * the HighScoreWriter is called upon to create a default score file.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/29/14
 */
public class HighScoreReader {

    /**
     * Reads the scores from the scores file and returns a sorted list of
     * the scores.
     *
     * @return a list of scores as read from the scores file
     */
    public List<HighScore> readScores()  {
        List<HighScore> scores = new ArrayList<HighScore>();
        File scoreFile = new File(System.getProperty("user.dir" ) +
                "/scores.txt");

        if (!scoreFile.exists()) {
            new HighScoreWriter().writeDefaultScores();
        }

        Scanner reader = null;
        try {
            reader = new Scanner(new FileReader(System.getProperty("user.dir")
                    + "/scores.txt"));
            String line;

            while (reader.hasNext()) {
                line = reader.nextLine();

                String name = line.split("\\|")[0];
                int score = Integer.parseInt(line.split("\\|")[1]);
                scores.add(new HighScore(name, score));
            }
            Collections.sort(scores, new ReverseComparator());

        } catch (FileNotFoundException e) {
            System.out.println("**** Failed to read file!");
            System.out.println(e.getMessage());
        }
        return scores;
    }
}
