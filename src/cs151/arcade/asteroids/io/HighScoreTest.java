package cs151.arcade.asteroids.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Tests that the HighScore reader and writer actually works
 *
 * @author Anthony Benavente
 * @author Daniel powell
 * @version 3/29/14
 */
public class HighScoreTest {
    static final String TEST_FILE = "scores_test.txt";
    static String[] names = ("Lilo,Stitch,Mulan,Mushu,Maximillian,Flynn," +
            "Elsa,Rapunzel,Ursula,Ariel,Sebastian,Triton,Simba,Nala,Mufasa," +
            "Timone,Pumba,Jay Gatsby,Bill Gates,Barack Obama,Steve Jobs," +
            "Charizard,Ash Ketchum,Joe Flacco").split(",");

    public static void main(String[] args) throws IOException {
//        testReadNonExistentFile();
//        testWriteToNonExistentFile();
        testReadNonExistentFile();
    }

    private static void testWriteToNonExistentFile() {
        File file = new File("res/scores/scores.txt");
        if (file.exists()) {
            file.delete();
        }

        HighScoreWriter writer = new HighScoreWriter();
        writer.writeScore("TEST", 10000);

        HighScoreReader reader = new HighScoreReader();
        List<HighScore> scores = null;
        scores = reader.readScores();
        for (HighScore score : scores) {
            System.out.println(score.formalString());
        }
    }

    private static void testReadNonExistentFile() {
        File file = new File("res/scores/scores.txt");
        if (file.exists()) {
            file.delete();
        }

        HighScoreReader reader = new HighScoreReader();
        List<HighScore> scores = reader.readScores();
        for (HighScore score : scores) {
            System.out.println(score.formalString());
        }
    }
}
