package cs151.arcade.asteroids.io;

import java.util.Comparator;

/**
 * Used when ordering the high score file in reverse order (from
 * highest-lowest).
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/29/14
 */
public class ReverseComparator implements Comparator<HighScore> {

    /**
     * Returns a positive, negative, or 0 number if highScore2 is higher,
     * lower, or equal to highScore.
     *
     * @param highScore the high score to compare with
     * @param highScore2 the high score to compare to
     * @return a positive number if highScore2 is greater than highScore,
     * a negative number if highScore2 is less than highScore,
     * or 0 if the two HighScores are equal
     */
    @Override
    public int compare(HighScore highScore, HighScore highScore2) {
        return highScore2.getScore() - highScore.getScore();
    }
}
