package cs151.arcade.asteroids.main;

import cs151.arcade.asteroids.states.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * The game that runs the individual states
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class Asteroids extends StateBasedGame {

    /**
     * The width of the original game before being scaled
     */
    public static final int GAME_WIDTH = 1366;

    /**
     * The height of the original game before being scaled
     */
    public static final int GAME_HEIGHT = 1024;

    /**
     * Create a new state based game
     *
     * @param name The name of the game
     */
    public Asteroids(String name) {
        super(name);
    }

    /**
     * Initialise the list of states making up this game
     *
     * @param container The container holding the game
     * @throws org.newdawn.slick.SlickException Indicates a failure to initialise the state based game resources
     */
    @Override
    public void initStatesList(GameContainer container)
            throws SlickException {
        addState(new MainMenuState(this));
        addState(new GameplayState());
        addState(new QuitState());
        addState(new HighScoreState(this));
        enterState(States.MENU.getID());
    }

}
