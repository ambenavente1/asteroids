package cs151.arcade.asteroids.objects;

import cs151.arcade.asteroids.objects.intefaces.Renderable;
import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

/**
 * An object in the game that is drawn to the screen with either an image
 * or an Animation (from Slick2D)
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public abstract class AnimatedObject extends StaticObject implements
        Renderable {

    /**
     * The animation to draw for this object
     */
    private Animation animation;

    /**
     * If the animation for this object should be used or not
     */
    private boolean toAnimate;

    /**
     * Creates a new AnimatedObject
     *
     * @param x the starting x coordinate
     * @param y the starting y coordinate
     */
    public AnimatedObject(float x, float y) {
        super(x, y);

        toAnimate = true;
    }

    /**
     * This is where you initialize the animation for the object.  Remember
     * that the object also has a static image so you still need to call
     * initImage()
     */
    protected abstract void initAnimation();

    /**
     * Draws the object to the screen by drawing this object's assigned
     * image
     *
     * @param container The container holding the game
     * @param game      The game itself (has the ability to change states)
     * @param g         The graphics object used to render images
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (toAnimate) {
            animation.draw(getX(), getY(), getWidth(), getHeight());
        } else {
            super.render(container, game, g);
        }
    }

    /**
     * Gets the animation used when drawing this object
     *
     * @return the animation drawn to represent this object
     */
    public Animation getAnimation() {
        return animation;
    }

    /**
     * Gets if this object should be animated or not
     *
     * @return if this object should be animated
     */
    protected boolean toAnimate() {
        return toAnimate;
    }

    /**
     * Sets if this object should be animated or not
     *
     * @param toAnimate whether the object should be animated or not
     */
    protected void setToAnimate(boolean toAnimate) {
        this.toAnimate = toAnimate;
    }
}
