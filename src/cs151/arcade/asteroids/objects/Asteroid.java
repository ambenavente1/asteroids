package cs151.arcade.asteroids.objects;

import cs151.arcade.asteroids.collisions.Collidable;
import cs151.arcade.asteroids.collisions.CollideEventArgs;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.util.MyRandom;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Represents an asteroid in outer space!
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class Asteroid extends StaticObject implements Updatable, Collidable {

    /**
     * The lowest mass a GIGANTIC asteroid can be
     */
    private static final int GIGANTIC_MASS = 1200;

    /**
     * The lowest mass a LARGE asteroid can be
     */
    private static final int LARGE_MASS = 800;

    /**
     * The lowest mass a MEDIUM asteroid can be
     */
    private static final int MEDIUM_MASS = 400;

    /**
     * Random number generator used for random stuff
     */
    private static MyRandom rand = new MyRandom();

    /**
     * Size of the asteroid fit into the four sizes
     */
    private AsteroidSize size;

    /**
     * Speed that the asteroid rotates at
     */
    private float rotateSpeed;

    /**
     * Speed that the asteroid moves at
     */
    private float movementSpeed;

    /**
     * If this asteroid's health had dropped below 0 and it should be
     * removed from the asteroid update queue
     */
    private boolean dead;

    /**
     * Creates an Asteroid object with a starting position but a random size
     *
     * @param x the starting x coordinate
     * @param y the starting y coordinate
     */
    public Asteroid(float x, float y) {
        this(x, y, AsteroidSize.values()[rand.nextInt(AsteroidSize.values()
                .length)]);
    }

    /**
     * Creates a new Asteroid object with a starting position and a
     * specified size
     *
     * @param x    the starting x coordinate
     * @param y    the starting y coordinate
     * @param size the size the asteroid should be
     */
    public Asteroid(float x, float y, AsteroidSize size) {
        super(x, y);
        this.size = size;
        this.rotateSpeed = rand.nextFloat(-2.6f, 2.6f);
        this.movementSpeed = rand.nextFloat(-1.55f, 1.55f);

        this.rotateSpeed *= rand.nextBoolean() ? -1 : 1;
        this.movementSpeed *= rand.nextBoolean() ? -1 : 1;
        this.dead = false;

        int upperMass = 0;
        int lowerMass = 0;
        switch (size) {
            case SMALL:
                lowerMass = 100;
                upperMass = MEDIUM_MASS;
                break;
            case MEDIUM:
                lowerMass = MEDIUM_MASS;
                upperMass = LARGE_MASS;
                break;
            case LARGE:
                lowerMass = LARGE_MASS;
                upperMass = GIGANTIC_MASS;
                break;
            case GIGANTIC:
                lowerMass = GIGANTIC_MASS;
                upperMass = (int) (GIGANTIC_MASS * 1.5f);
                break;
        }
        setMass(rand.nextInt(lowerMass, upperMass));
        classifySize(getMass());
        setHealth(getMass());
        setFacingAngle(rand.nextFloat(0f, 360f));
        setMovementAngle(rand.nextFloat(0f, 360f));

        initImage();

        if (getImage() != null) {
            setWidth((int) (getImage().getWidth() * size.getScaleFactor()));
            setHeight((int) (getImage().getHeight() * size.getScaleFactor()));
        } else {
            setWidth(getSize().ordinal() >= AsteroidSize.LARGE.ordinal() ?
                    136 : 44);
            setHeight(getSize().ordinal() >= AsteroidSize.LARGE.ordinal() ?
                    111 : 42);

        }
    }

    /**
     * Classifies the size of this asteroid based on its mass
     *
     * @param mass the mass of this asteroid
     */
    private void classifySize(float mass) {
        if (mass > 1200) {
            size = AsteroidSize.GIGANTIC;
        } else if (mass > 800) {
            size = AsteroidSize.LARGE;
        } else if (mass > 400) {
            size = AsteroidSize.MEDIUM;
        } else {
            size = AsteroidSize.SMALL;
        }
    }

    /**
     * Load the image for the object in this method.  Be sure to adjust
     * dimensions in here as well... (it is in this method because you can
     * change the dimensions based on the image's dimensions)
     */
    @Override
    protected void initImage() {
        try {
            String imageRef = size.ordinal() >= AsteroidSize.LARGE.ordinal() ?
                    "res/images/meteorLarge.png" : "res/images/meteorSmall" +
                    ".png";
            setImage(new Image(imageRef));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the size of this asteroid
     *
     * @return the size of this asteroid
     */
    public AsteroidSize getSize() {
        return size;
    }

    /**
     * Called when this asteroid collides with either a bullet of a ship
     *
     * @param e arguments for another object colliding with this one
     */
    @Override
    public void onCollide(CollideEventArgs e) {
        setHealth(getHealth() - e.getDamage());
        dead = getHealth() <= 0;
    }

    /**
     * Updates the asteroid
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        setFacingAngle(getFacingAngle() + rotateSpeed);
        setRotation(getFacingAngle());

        Vector2f tmp = new Vector2f((float) Math.cos(getMovementAngle()) *
                2.3f, (float) Math.sin(getMovementAngle()) * 2.3f);

        setVel(tmp);

        setPos(getVel().copy().add(getPos().copy()));
    }

    /**
     * Gets if this asteroid is dead or not
     *
     * @return if this asteroid is dead
     */
    public boolean isDead() {
        return dead;
    }
}
