package cs151.arcade.asteroids.objects;

import cs151.arcade.asteroids.util.MyRandom;

/**
 * A list of sizes that asteroids could be
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/27/14
 */
public enum AsteroidSize {

    /**
     * Small asteroid that is scaled down by 25%
     */
    SMALL(0.75f),

    /**
     * Medium asteroid that is not scaled
     */
    MEDIUM(1.0f),

    /**
     * Large asteroid that is not scaled
     */
    LARGE(1.0f),

    /**
     * Gigantic asteroid scaled up by 25%
     */
    GIGANTIC(1.25f);

    /**
     * Random number generator for returning a random size
     */
    private static MyRandom rand = new MyRandom();

    /**
     * Amount to scale the asteroid's image
     */
    private float scaleFactor;

    /**
     * Creates a new AsteroidSize enum indicating the amount to scale the
     * asteroid's size
     *
     * @param scaleFactor the amount to scale the asteroid's default size
     */
    private AsteroidSize(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    /**
     * Gets a random AsteroidSize enumeration
     *
     * @return a random AsteroidSize
     */
    public static AsteroidSize getRandom() {
        return getRandom(SMALL);
    }

    /**
     * Gets a random asteroid size with a lower boundary only
     *
     * @param low the smallest size to return
     * @return a random asteroid size between the specified low size and
     * the highest size of the enum
     */
    public static AsteroidSize getRandom(AsteroidSize low) {
        return getRandom(low.ordinal(), values().length);
    }

    /**
     * Gets a random AsteroidSize within a range of ordinals
     *
     * @param low  lowest ordinal
     * @param high highest ordinal
     * @return a random AsteroidSize within the range given
     */
    public static AsteroidSize getRandom(int low, int high) {
        return values()[rand.nextInt(low, high)];
    }

    /**
     * Gets the amount to scale the asteroid's size
     *
     * @return the amount to scale the asteroid's size
     */
    public float getScaleFactor() {
        return scaleFactor;
    }
}
