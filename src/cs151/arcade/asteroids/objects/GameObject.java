package cs151.arcade.asteroids.objects;

import cs151.arcade.asteroids.main.Asteroids;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

/**
 * Represents an object in the game that can be interacted with or
 * interacts with another game element.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class GameObject {

    /**
     * The fastest any object can reach in the game
     */
    protected static final float TERMINAL_VEL = 9.5F;

    /**
     * The position of the object
     */
    private Vector2f pos;

    /**
     * The width of the object in pixels
     */
    private int width;

    /**
     * The height of the object in pixels
     */
    private int height;

    /**
     * The amount of health this object has left until it is considered dead
     */
    private float health;

    /**
     * The mass of this object
     */
    private float mass;

    /**
     * The vector representing the velocity of this object
     */
    private Vector2f vel;

    /**
     * The vector representing the acceleration of this object
     */
    private Vector2f accel;

    /**
     * The angle that this object is moving at
     */
    private float movementAngle;

    /**
     * The angle that this object is facing
     */
    private float facingAngle;

    /**
     * A rectangle representing the bounds of this object
     */
    private Rectangle bounds;

    /**
     * Creates a default game object with a position of 0,0,
     * a width and height of 0, and a mass of 0
     */
    public GameObject() {
        this(0, 0);
    }

    /**
     * Creates a game object with a specified position,
     * but a default width, height, and mass of 0
     *
     * @param x the starting x coordinate
     * @param y the starting y coordinate
     */
    public GameObject(float x, float y) {
        this(x, y, 0, 0);
    }

    /**
     * Creates a new GameObject with a specified location, width,
     * and height, but a default mass of 0
     *
     * @param x      the the starting x coordinate
     * @param y      the starting y coordinate
     * @param width  the width of the object
     * @param height the height of the object
     */
    public GameObject(float x, float y, int width, int height) {
        this(new Vector2f(x, y), width, height, 0);
    }

    /**
     * Creates a new GameObject with all fields specified
     *
     * @param pos    the starting position
     * @param width  the width of the object
     * @param height the height of the object
     * @param mass   the mass of the object
     */
    public GameObject(Vector2f pos, int width, int height, float mass) {
        this.pos = pos;
        this.width = width;
        this.height = height;
        this.mass = mass;
        this.health = 100;
        this.movementAngle = 0;
        this.facingAngle = 0;
        this.vel = new Vector2f(0, 0);
        this.accel = vel.copy();
        this.bounds = new Rectangle(pos.x, pos.y, width, height);
    }

    /**
     * Gets the health of the object
     *
     * @return the health of the object
     */
    public float getHealth() {
        return health;
    }    /**
     * Gets the position of the object
     *
     * @return the position of the object
     */
    public Vector2f getPos() {
        return pos;
    }

    /**
     * Sets the new health of this object.
     *
     * @param health the new health of this object
     */
    protected void setHealth(float health) {
        this.health = health;
    }    /**
     * Gets the width of the object
     *
     * @return the width of the object
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the mass of the object
     *
     * @return the mass of the object
     */
    public float getMass() {
        return mass;
    }    /**
     * Gets the height of the object
     *
     * @return the height of the object
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the new mass for this object.
     *
     * @param mass the new mass for this object
     */
    protected void setMass(float mass) {
        this.mass = mass;
    }

    /**
     * Gets the velocity of this object.
     *
     * @return the velocity of the object
     */
    public Vector2f getVel() {
        return vel;
    }

    /**
     * Sets the velocity of the object
     *
     * @param vel the new velocity of the object
     */
    protected void setVel(Vector2f vel) {
        this.vel = vel;
    }    /**
     * Sets the new position of the object.  After setting position,
     * the game wraps the position around the game bounds,
     * and then updates the objects rectangular bounding box.
     *
     * @param pos the new position of the object
     */
    public void setPos(Vector2f pos) {
        this.pos = pos;
        wrapPos();
        updateBounds();
    }

    /**
     * Gets the angle of movement that this object is following.
     *
     * @return the angle of movement of this object
     */
    public float getMovementAngle() {
        return movementAngle;
    }    /**
     * Sets the new width of the object.  After setting width,
     * the game updates the objects rectangular bounding box.
     *
     * @param width the new width of the object
     */
    protected void setWidth(int width) {
        this.width = width;
        updateBounds();
    }

    /**
     * Sets the angle of movement of this object and wraps it around 360
     *
     * @param movementAngle the new angle of movement for this object
     */
    protected void setMovementAngle(float movementAngle) {
        this.movementAngle = movementAngle % 360f;
    }    /**
     * Sets the new height of the object.  After setting height,
     * the game updates the objects rectangular bounding box.
     *
     * @param height the new height of the object
     */
    protected void setHeight(int height) {
        this.height = height;
        updateBounds();
    }

    /**
     * Gets the angle that this object is facing
     *
     * @return the angle that the object is facing
     */
    public float getFacingAngle() {
        return facingAngle;
    }

    /**
     * Sets the angle that this object is facing and wraps it around 360
     *
     * @param facingAngle the new angle that this object is facing
     */
    protected void setFacingAngle(float facingAngle) {
        this.facingAngle = facingAngle % 360f;
    }

    /**
     * Adds an amount of degrees to the object's facing angle (rotates the
     * object)
     *
     * @param amount the amount to rotate this object
     */
    protected void addToFacingAngle(float amount) {
        setFacingAngle(facingAngle + amount);
    }    /**
     * Sets the x component of this objects position.
     *
     * @param x the new x component of this object's position
     */
    public void setX(float x) {
        setPos(new Vector2f(x, getPos().y));
    }

    /**
     * The centered position of this object
     *
     * @return the center of this object in relation to the world
     */
    public Vector2f centerPos() {
        return new Vector2f(pos.x + width / 2, pos.y + height / 2);
    }    /**
     * Sets the new y component of this object's position.
     *
     * @param y the new y component of this object's position
     */
    public void setY(float y) {
        pos.y = y;
        wrapPos();
        updateBounds();
    }

    /**
     * Gets the rectangular area representing the collision area for this
     * object
     *
     * @return the bounding box of this object
     */
    public Rectangle getBounds() {
        return bounds;
    }    /**
     * Gets the x coordinate of this object's position
     *
     * @return the x coordinate of this object's position
     */
    public float getX() {
        return pos.getX();
    }

    /**
     * Applies a force to this object's acceleration taking into account
     * the objects mass as FORCE * MASS = ACCELERATION.
     *
     * @param force amount of force to apply
     */
    protected void applyForce(Vector2f force) {
        accel.add(force.scale(1 / mass));
    }    /**
     * Gets the y coordinate of this object's position
     *
     * @return the y coordinate of this object's position
     */
    public float getY() {
        return pos.getY();
    }

    /**
     * Applies the velocity to the object's position
     */
    protected void applyVelocity() {
        setPos(pos.copy().add(vel));
    }

    /**
     * Applies the acceleration to the object's velocity and limits the
     * velocity to the terminal velocity
     */
    protected void applyAccel() {
        setVel(vel.copy().add(accel));

        if (vel.x > TERMINAL_VEL) vel.x = TERMINAL_VEL;
        else if (vel.x < -TERMINAL_VEL) vel.x = -TERMINAL_VEL;

        if (vel.y > TERMINAL_VEL) vel.y = TERMINAL_VEL;
        else if (vel.y < -TERMINAL_VEL) vel.y = -TERMINAL_VEL;
    }

    /**
     * Sets the acceleration of this object
     *
     * @param accel the new acceleration of this object
     */
    protected void setAccel(Vector2f accel) {
        this.accel = accel;
    }













    /**
     * Updates the bounding box of this object when dimensions or location
     * changes
     */
    private void updateBounds() {
        bounds.setBounds(getX(), getY(), getWidth(), getHeight());
    }









    /**
     * Wraps this object's position around the game bounds
     */
    private void wrapPos() {
        if (pos.x < -getWidth()) {
            pos.x = Asteroids.GAME_WIDTH;
        } else {
            pos.x %= Asteroids.GAME_WIDTH;
        }

        if (pos.y < -getHeight()) {
            pos.y = Asteroids.GAME_HEIGHT;
        } else {
            pos.y %= Asteroids.GAME_HEIGHT;
        }
    }
}
