package cs151.arcade.asteroids.objects;

import cs151.arcade.asteroids.collisions.Collidable;
import cs151.arcade.asteroids.collisions.CollideEventArgs;
import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;
import cs151.arcade.asteroids.weapons.guns.LaserRifle;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Represents a player-controlled ship that has the ability to shoot
 * asteroids, respawn, die, collide with asteroids, and more.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public class Ship extends StaticObject implements Updatable, Collidable {

    /**
     * Amount force exerted upon by the ship's thrusters
     */
    private static final float THRUST_FORCE = 120.5F;

    /**
     * How fast the ship rotates
     */
    private static final float ROTATE_SPEED = 4.4F;

    /**
     * How much the ship's original size should be scaled down to
     */
    private static final float SCALE = .55F;

    /**
     * The specific max velocity for the ship (lower than the terminal
     * velocity)
     */
    private static final float MAX_VEL = 7.5F;

    /**
     * Amount of time the ship is invincible
     */
    private static final int MAX_DEAD_TIME = 2000;

    /**
     * The weapon the ship uses to destroy asteroids
     */
    private Weapon weapon;

    /**
     * If the ship had just hit an asteroid
     */
    private boolean dead;

    /**
     * The amount of time the ship has been invincible
     */
    private int deadTime;

    /**
     * The number of lives the ship has (starts at 3)
     */
    private int lives;

    /**
     * The ship's score
     */
    private int points;

    /**
     * Creates a default ship object
     */
    public Ship() {
        super(0, 0);
        initImage();
        if (getImage() != null) {
            setWidth(getImage().getWidth());
            setHeight(getImage().getHeight());
        } else {
            setWidth(75);
            setHeight(99);
        }
        setMass(600);
        setHealth(getMass());

        weapon = new LaserRifle();
        dead = false;
        lives = 3;
        points = 0;

        reset();
    }

    /**
     * Initializes the ship's image
     */
    @Override
    protected void initImage() {
        try {
            setImage(new Image("res/images/player.png").getScaledCopy(SCALE));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Renders the ship
     *
     * @param container The container holding the game
     * @param game      The game itself (has the ability to change states)
     * @param g         The graphics object used to render images
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        if (!isGameOver()) {
            weapon.render(container, game, g);

            if (isInvincible()) {
                if (deadTime % 200 > 100) {
                    super.render(container, game, g);
                }
            } else {
                super.render(container, game, g);
            }

            String msg = "Lives: " + lives;
            g.drawString("Lives: " + lives,
                    Asteroids.GAME_WIDTH - g.getFont().getWidth(msg) - 10,
                    Asteroids.GAME_HEIGHT - g.getFont().getLineHeight() - 10);
        }
    }

    /**
     * Gets if the ship is invincible or not
     *
     * @return if the ship is invincible
     */
    public boolean isInvincible() {
        return deadTime >= 0;
    }

    /**
     * Resets the ship's weapon, position, facing angle, and velocity
     */
    public void reset() {
        setX(Asteroids.GAME_WIDTH / 2 - getWidth() / 2);
        setY(Asteroids.GAME_HEIGHT / 2 - getHeight() / 2);
        setFacingAngle(-90f);
        setVel(getVel().scale(0));
        weapon.clear();
    }

    /**
     * Updates the ship
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        if (!isGameOver()) {
            if (deadTime >= 0) deadTime -= delta;

            Input input = container.getInput();

            if (input.isKeyDown(Input.KEY_LEFT)) {
                addToFacingAngle(-ROTATE_SPEED);
            }

            if (input.isKeyDown(Input.KEY_RIGHT)) {
                addToFacingAngle(ROTATE_SPEED);
            }

            if (getImage() != null) {
                getImage().setRotation(getFacingAngle());
            }

            if (input.isKeyDown(Input.KEY_UP)) {
                setMovementAngle(getFacingAngle());
                float thrustX = (float) (THRUST_FORCE * Math.cos(
                        Math.toRadians(getMovementAngle()))
                );
                float thrustY = (float) (THRUST_FORCE * Math.sin(
                        Math.toRadians(getMovementAngle()))
                );
                Vector2f thrust = new Vector2f(thrustX, thrustY);
                applyForce(thrust);
            }

            applyAccel();
            applyVelocity();
            limitVelocity();
            setAccel(new Vector2f());

            weapon.update(container, game, delta);
            if (input.isKeyDown(Input.KEY_SPACE)) {
                weapon.onFire(new PlayerEventArgs(this));
            }

            if (dead) {
                dead = false;
                lives--;
                deadTime = MAX_DEAD_TIME;
                reset();
            }
        }
//        System.out.println("Position: " + getPos() + "\n" +
//                           "Moving Angle: " + getMovementAngle() + "\n" +
//                           "Facing Angle: " + getFacingAngle());
    }

    /**
     * Gets if the ship is out of lives
     *
     * @return if the ship is out of lives
     */
    public boolean isGameOver() {
        return lives <= 0;
    }

    /**
     * Limits the ship's velocity to the specified ship's max velocity
     */
    private void limitVelocity() {
        if (getVel().x < -MAX_VEL) {
            setVel(new Vector2f(-MAX_VEL, getVel().y));
        } else if (getVel().x > MAX_VEL) {
            setVel(new Vector2f(MAX_VEL, getVel().y));
        }

        if (getVel().y < -MAX_VEL) {
            setVel(new Vector2f(getVel().x, -MAX_VEL));
        } else if (getVel().x > MAX_VEL) {
            setVel(new Vector2f(getVel().x, -MAX_VEL));
        }
    }

    /**
     * Gets the weapon used by this ship
     *
     * @return this ship's weapon
     */
    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Gets if this ship had just hit an asteroid
     *
     * @return if or if not the ship had just hit an asteroid and is
     * invincible
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Called when this ship collides with an asteroid
     *
     * @param e arguments for another object colliding with this one
     */
    @Override
    public void onCollide(CollideEventArgs e) {
        setHealth(getHealth() - e.getMass());
        dead = getHealth() <= 0;
    }

    /**
     * Gets the number of lives this ship has
     *
     * @return the number of lives this ship has
     */
    public int getLives() {
        return lives;
    }

    /**
     * Adds a number of points to this ship's points
     *
     * @param points the number of points to add
     */
    public void addPoints(int points) {
        this.points += points;
    }

    /**
     * Gets the number of points this ship has scored
     *
     * @return the number of points of this ship
     */
    public int getPoints() {
        return points;
    }

    /**
     * Sets the ship's weapon
     *
     * @param weapon the weapon to give to the ship
     */
    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public void addLife() {
        lives++;
    }
}
