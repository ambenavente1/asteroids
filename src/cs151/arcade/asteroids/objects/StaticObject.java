package cs151.arcade.asteroids.objects;


import cs151.arcade.asteroids.objects.intefaces.Renderable;
import cs151.arcade.asteroids.weapons.bullets.Bullet;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.state.StateBasedGame;

/**
 * An object in our game that is drawn to the screen with a static image.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public abstract class StaticObject extends GameObject implements Renderable {

    /**
     * Image used to represent this object (drawn to the screen)
     */
    private Image image;

    /**
     * Creates a static object with specified position
     *
     * @param x the starting x coordinate
     * @param y the starting y coordinate
     */
    public StaticObject(float x, float y) {
        super(x, y);
    }

    /**
     * Load the image for the object in this method.  Be sure to adjust
     * dimensions in here as well... (it is in this method because you can
     * change the dimensions based on the image's dimensions)
     */
    protected abstract void initImage();

    /**
     * Draws the object to the screen by drawing this object's assigned
     * image
     *
     * @param container The container holding the game
     * @param game      The game itself (has the ability to change states)
     * @param g         The graphics object used to render images
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (image != null) {
            image.draw(getX(), getY(), getWidth(), getHeight());
        } else {
            Class thisClass = this.getClass();

            if (thisClass == Asteroid.class) {
                g.drawOval(getX(), getY(), getWidth(), getHeight());
            } else if (thisClass == Ship.class) {
                Polygon polygon = new Polygon();
                polygon.addPoint(getX() + getWidth(),
                        getY() + getHeight() / 2);
                polygon.addPoint(getX(), getY());
                polygon.addPoint(getX(), getY() + getHeight());

                g.rotate(getX() + getWidth() / 3, getY() + getHeight() / 3,
                        getFacingAngle());
                g.draw(polygon);
                g.rotate(getX() + getWidth() / 3, getY() + getHeight() / 3,
                        -getFacingAngle());
            } else if (this instanceof Bullet) {
                g.rotate(getX() + getWidth() / 2, getY() + getHeight() / 2,
                        getFacingAngle());
                g.drawRect(getX(), getY(), getWidth(), getHeight());
                g.rotate(getX() + getWidth() / 2, getY() + getHeight() / 2,
                        -getFacingAngle());
            }
        }
//        for debugging:
//        g.drawString("Movement angle: " + getMovementAngle(),
//                     getX(),
//                     getY());
//        g.drawString("Facing angle: " + getFacingAngle(),
//                     getX(),
//                     getY() + 30);
    }

    /**
     * Gets the image object used when drawing this object
     *
     * @return the image of this object
     */
    public Image getImage() {
        return image;
    }

    /**
     * Sets the image of this object that is drawn in the render method
     *
     * @param image the image representing this object
     */
    protected void setImage(Image image) {
        this.image = image;
    }

    /**
     * Sets the angle of rotation for this image
     *
     * @param facingAngle the amount to rotate the image
     */
    protected void setRotation(float facingAngle) {
        if (image != null) {
            image.setRotation(facingAngle);
        }
    }
}
