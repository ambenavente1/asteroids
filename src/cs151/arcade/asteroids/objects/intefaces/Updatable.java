package cs151.arcade.asteroids.objects.intefaces;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Used to indicate that whatever implements this can be updated by the
 * game loop.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public interface Updatable {

    /**
     * Where the actual game logic updates for this object should be
     * getting called at.
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     *                  last update
     */
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta);
}
