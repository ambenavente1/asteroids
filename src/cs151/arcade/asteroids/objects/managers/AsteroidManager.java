package cs151.arcade.asteroids.objects.managers;

import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.objects.Asteroid;
import cs151.arcade.asteroids.objects.AsteroidSize;
import cs151.arcade.asteroids.objects.intefaces.Renderable;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 * This manager is in charge of updating and rendering all the asteroids in
 * the game.  This class is iterable and has a built-in iterator to make
 * this class applicable to for each loops without allowing the ability to
 * change the list (remove is not supported for the iterator).
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class AsteroidManager implements Updatable,
        Renderable, Iterable<Asteroid> {

    /**
     * A list containing all the asteroids
     */
    private List<Asteroid> asteroids;

    /**
     * The current level that the player is at.  As this increases,
     * the number of asteroids at the start of the level increases.
     */
    private int level;

    /**
     * Random number generator for use in various randomized things
     */
    private Random rand;

    /**
     * Creates a default AsteroidManager object starting at level 1
     */
    public AsteroidManager() {
        asteroids = new ArrayList<Asteroid>();
        rand = new Random();
        level = 1;
        restart();
    }

    /**
     * Adds asteroids with a formula to the asteroid list at random
     * locations (not too close to the player)
     */
    public void restart() {
        addRandomAsteroid((int) (2 + Math.round((level - 1) * 1.125)),
                AsteroidSize.getRandom(AsteroidSize.LARGE));
    }

    /**
     * Adds a number of random asteroids to the asteroid list
     *
     * @param amount the amount to add
     * @param size   the size of the asteroid to add
     */
    private void addRandomAsteroid(int amount, AsteroidSize size) {
        Vector2f center = new Vector2f(Asteroids.GAME_WIDTH / 2,
                Asteroids.GAME_HEIGHT / 2);
        for (int i = 0; i < amount; i++) {
            Asteroid asteroid = new Asteroid(0, 0, size);
            do {
                float randX = rand.nextInt(Asteroids.GAME_WIDTH - asteroid
                        .getWidth());
                float randY = rand.nextInt(Asteroids.GAME_HEIGHT - asteroid
                        .getHeight());
                asteroid.setX(randX);
                asteroid.setY(randY);
            } while (asteroid.getPos().distance(center) < 400);
            asteroids.add(asteroid);
        }
    }

    /**
     * Entry point to the iterator test
     *
     * @param args no arguments are used
     * @throws SlickException
     */
    public static void main(String[] args) throws SlickException {
        AppGameContainer appgc = new AppGameContainer(new
                AsteroidManagerTest("Asteroid Manager Test"));
        appgc.start();
    }

    /**
     * Where the actual game logic updates for this object should be
     * getting called at.
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        for (int i = asteroids.size() - 1; i >= 0; i--) {
            Asteroid current = asteroids.get(i);

            if (current.isDead()) {
                splitAsteroid(current);
                asteroids.remove(current);
            } else {
                current.update(container, game, delta);
            }
        }
    }

    /**
     * Splits an asteroid depending on its size.  If the asteroid is large,
     * it splits into 2 small asteroids.  If the asteroid is gigantic,
     * it splits into 4 small asteroids.
     *
     * @param a the asteroid to split
     */
    private void splitAsteroid(Asteroid a) {
        if (a.getSize() == AsteroidSize.LARGE ||
                a.getSize() == AsteroidSize.GIGANTIC) {

            AsteroidSize size = AsteroidSize.SMALL;

            // TODO: Add 2 - 3 asteroids
            asteroids.add(new Asteroid(a.getX(), a.getY(), size));
            asteroids.add(new Asteroid(a.getX(), a.getY(), size));

            if (a.getSize() == AsteroidSize.GIGANTIC) {
                asteroids.add(new Asteroid(a.getX(), a.getY(), size));
                asteroids.add(new Asteroid(a.getX(), a.getY(), size));
            }
        }
    }

    /**
     * The method where the rendering of this object should take place.
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        for (Asteroid a : asteroids) {
            a.render(container, game, g);
        }
    }

    /**
     * Increases the level by 1
     */
    public void levelUp() {
        level++;
    }

    /**
     * Checks if the asteroid list is empty
     *
     * @return if the size of the asteroid list is 0
     */
    public boolean isEmpty() {
        return asteroids.size() == 0;
    }

    /**
     * Gets an iterator to walk through the asteroid list
     *
     * @return an iterator to walk through the asteroid list
     */
    @Override
    public Iterator<Asteroid> iterator() {
        return new AsteroidManagerIterator();
    }

    /**
     * Small test game to test the iterator when using a foreach loop to
     * update the asteroid list
     */
    private static class AsteroidManagerTest extends BasicGame {

        /**
         * AsteroidManager to test
         */
        AsteroidManager asteroids1;

        public AsteroidManagerTest(String title) {
            super(title);
        }

        /**
         * Initializes the asteroid manager
         *
         * @param gameContainer container holding the game
         * @throws SlickException if there was an error with Slick2D
         */
        @Override
        public void init(GameContainer gameContainer) throws SlickException {
            asteroids1 = new AsteroidManager();
        }

        /**
         * Where the actual game logic updates for this object should be
         * getting called at.
         *
         * @param container the container holding the game
         * @param delta     the amount of time that's passed in millisecond since
         * @throws SlickException
         */
        @Override
        public void update(GameContainer container,
                           int delta) throws SlickException {
            for (Asteroid a : asteroids1) {
                a.update(container, null, delta);
            }
        }

        /**
         * The method where the rendering of this object should take place.
         *
         * @param container the container holding the game
         * @param g         the graphics context to render to
         * @throws SlickException
         */
        @Override
        public void render(GameContainer container, Graphics g) throws
                SlickException {
            for (Asteroid a : asteroids1) {
                a.render(container, null, g);
            }
        }
    }

    /**
     * Iterates through the asteroid list in a forward direction.  <em>NOTE:
     * This iterator does not support removing items while iterating.</em>
     */
    private class AsteroidManagerIterator implements Iterator<Asteroid> {

        /**
         * Index of iteration starts at 0
         */
        private int i = 0;

        /**
         * Checks if there are still elements to iterate over
         *
         * @return true if there are still elements, false otherwise
         */
        @Override
        public boolean hasNext() {
            return i < asteroids.size();
        }

        /**
         * Returns the element that the iterator jumps over
         *
         * @return the element that was just jumped over by the iterator
         */
        @Override
        public Asteroid next() {
            return asteroids.get(i++);
        }

        /**
         * REMOVE IS NOT SUPPORTED FOR THIS ITERATOR!
         */
        @Override
        public void remove() {
            throw new RuntimeException("Remove is not supported!");
        }
    }
}
