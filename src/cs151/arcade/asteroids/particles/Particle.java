package cs151.arcade.asteroids.particles;

import cs151.arcade.asteroids.objects.StaticObject;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * A small texture drawn to the screen representing a small particle
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/31/14
 */
public class Particle extends StaticObject implements Updatable {

    /**
     * Image used to represent a particle
     */
    private static Image PARTICLE;

    /**
     * Initializes the particle texture
     */
    static {
        try {
            PARTICLE = new Image("res/images/particle.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Amount of time this particle has been "live"
     */
    private float elapsed;

    /**
     * Amount of time this particle should last
     */
    private float duration;

    /**
     * Opacity of this particle's image
     */
    private float opacity;

    /**
     * Tint for this particle
     */
    private Color color;

    /**
     * Creates a new particle
     *
     * @param x        starting x coordinate
     * @param y        starting y coordinate
     * @param width    width of the particle
     * @param height   height of the particle
     * @param vX       initial x velocity of this particle
     * @param vY       initial y velocity of this particle
     * @param duration time that this particle should last on screen
     * @param color    tint overlay for this particle
     */
    public Particle(float x,
                    float y,
                    int width,
                    int height,
                    float vX,
                    float vY,
                    float duration,
                    Color color) {
        super(x, y);
        initImage();
        setWidth(width);
        setHeight(height);
        setVel(new Vector2f(vX, vY));
        getImage().setImageColor(color.r, color.g, color.b);
        this.color = color;
        this.duration = duration;
        this.elapsed = 0;
        this.opacity = 1.0f;
    }

    /**
     * Load the image for the object in this method.  Be sure to adjust
     * dimensions in here as well... (it is in this method because you can
     * change the dimensions based on the image's dimensions)
     */
    @Override
    protected void initImage() {
        setImage(PARTICLE.copy());
    }

    /**
     * Renders a shared particle texture
     *
     * @param container The container holding the game
     * @param game      The game itself (has the ability to change states)
     * @param g         The graphics object used to render images
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (!isDead()) {
            g.drawImage(PARTICLE, getX(), getY(), color);
        }
    }

    /**
     * Gets if this particle has disappeared from the screen
     *
     * @return if this particle's opacity is <= 0
     */
    public boolean isDead() {
        return opacity <= 0;
    }

    /**
     * Updates the particle by updating its position and opacity
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        if (!isDead()) {
            applyVelocity();
            opacity = (duration - (elapsed += delta)) / duration;
            getImage().setImageColor(color.r, color.g, color.b, opacity);
        }
    }
}
