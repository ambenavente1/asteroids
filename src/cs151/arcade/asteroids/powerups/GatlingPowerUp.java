package cs151.arcade.asteroids.powerups;

import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.weapons.guns.LaserGatlingGun;
import cs151.arcade.asteroids.weapons.guns.LaserRifle;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * This PowerUp gives the player a LaserGatlingGun weapon.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/17/14
 */
public class GatlingPowerUp extends PowerUp {

    /**
     * Creates a static object with specified position
     *
     * @param x        the starting x coordinate
     * @param y        the starting y coordinate
     */
    public GatlingPowerUp(float x, float y) {
        super(x, y, 10000);
        initImage();
        setWidth(getImage().getWidth());
        setHeight(getImage().getHeight());
    }

    /**
     * Gives the player a LaserGatlingGun
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    @Override
    public void startEffect(Ship ship) {
        ship.setWeapon(new LaserGatlingGun());
    }

    /**
     * Sets the player's weapon back to the LaserRifle.
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    @Override
    public void endEffect(Ship ship) {
        ship.setWeapon(new LaserRifle());
    }

    /**
     * Load the image for the object in this method.  Be sure to adjust
     * dimensions in here as well... (it is in this method because you can
     * change the dimensions based on the image's dimensions)
     */
    @Override
    protected void initImage() {
        try {
            setImage(new Image("res/powerups/Gatling.png")
                    .getScaledCopy(0.5f));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
