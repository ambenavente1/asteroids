package cs151.arcade.asteroids.powerups;

import cs151.arcade.asteroids.objects.Ship;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * This PowerUp gives the player one extra life.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/19/14
 */
public class LifePowerUp extends PowerUp {

    /**
     * Creates a static object with specified position
     *
     * @param x        the starting x coordinate
     * @param y        the starting y coordinate
     */
    public LifePowerUp(float x, float y) {
        // 0 because the power up has an instant effect
        super(x, y, 0);
        initImage();
        setWidth(getImage().getWidth());
        setHeight(getImage().getHeight());
    }

    /**
     * Nothing happens when the life power up gets collected
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    @Override
    public void startEffect(Ship ship) { }

    /**
     * Adds a life to the ship
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    @Override
    public void endEffect(Ship ship) { ship.addLife(); }

    /**
     * Load the image for the object in this method.  Be sure to adjust
     * dimensions in here as well... (it is in this method because you can
     * change the dimensions based on the image's dimensions)
     */
    @Override
    protected void initImage() {
        try {
            setImage(new Image("res/powerups/Life.png").getScaledCopy(0.5f));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
