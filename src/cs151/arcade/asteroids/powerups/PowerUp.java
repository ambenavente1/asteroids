package cs151.arcade.asteroids.powerups;

import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.objects.StaticObject;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.particles.ParticleSystem;
import cs151.arcade.asteroids.util.MyRandom;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import java.util.Random;

/**
 * A PowerUp is an object in the game that applies a certain effect on  the
 * ship.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/15/14
 */
public abstract class PowerUp extends StaticObject implements Updatable {

    private static MyRandom rand = new MyRandom();

    /**
     * The amount of time this power up has been active
     */
    private int elapsed;

    /**
     * The amount of time this power up lasts
     */
    private int duration;

    /**
     * If the power up is active or not (if the player has not yet
     * interacted/touched the power up)
     */
    private boolean active;

    /**
     * If the power up's effect ran out
     */
    private boolean dead;

    /**
     * Creates a static object with specified position
     *
     * @param x the starting x coordinate
     * @param y the starting y coordinate
     */
    public PowerUp(float x, float y, int duration) {
        super(x, y);
        this.elapsed    = 0;
        this.duration   = duration;
        this.active     = false;
        this.dead       = false;
        this.setVel(new Vector2f(rand.nextFloat(1, 3),
                rand.nextFloat(1, 3)));
        this.getVel().x *= rand.nextBoolean() ? 1 : -1;
        this.getVel().y *= rand.nextBoolean() ? 1 : -1;
    }

    /**
     * Called when the ship interacts with this power up
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    public abstract void startEffect(Ship ship);

    /**
     * Called when the power up has ran out
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    public abstract void endEffect(Ship ship);


    /**
     * Where the actual game logic updates for this object should be
     * getting called at.
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        if (active) {
            if ((elapsed += delta) >= duration) {
                // The power up should end
                dead = true;
            }
        } else {
            applyVelocity();
        }
    }

    /**
     * Draws the object to the screen by drawing this object's assigned
     * image
     *
     * @param container The container holding the game
     * @param game      The game itself (has the ability to change states)
     * @param g         The graphics object used to render images
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (!active) {
            super.render(container, game, g);
        }
    }

    /**
     * Starts the effect of this power up on the ship that used it
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    public void fireEffect(Ship ship) {
        startEffect(ship);
        active = true;
    }

    /**
     * Gets if the power up's effect has ran out
     *
     * @return true of false if the power up is done
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Gets if the power up is active in the ship
     *
     * @return if the power up is actively emitting its effect on the ship
     */
    public boolean isActive() {
        return active;
    }
}
