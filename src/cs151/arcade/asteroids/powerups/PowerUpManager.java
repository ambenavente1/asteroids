package cs151.arcade.asteroids.powerups;

import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.objects.intefaces.Renderable;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.util.MyRandom;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * PowerUpManager is in charge of spawning PowerUps in the game,
 * as well as, checking collisions with the player.
 * TODO: Move the collision check to CollisionManager
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/16/14
 */
public class PowerUpManager implements Updatable, Renderable {

    private static final int MAX_POWERUPS = 4;
    private static final int LOWEST_POWER_UP_TIME = 25000;
    private static final int HIGHEST_POWER_UP_TIME = 50000;

    private List<PowerUp> powerUps;
    private Ship ship;
    private MyRandom rand;
    private int elapsed;
    private int nextPowerUpTime;

    public PowerUpManager(Ship ship) {
        this.ship = ship;
        this.powerUps = new ArrayList<PowerUp>();
        this.rand = new MyRandom();
        this.elapsed = 0;
        this.nextPowerUpTime = rand.nextInt(LOWEST_POWER_UP_TIME,
                HIGHEST_POWER_UP_TIME);
    }

    /**
     * The method where the rendering of this object should take place.
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        for (int i = 0; i < powerUps.size(); i++) {
            powerUps.get(i).render(container, game, g);
        }
    }

    /**
     * Where the actual game logic updates for this object should be
     * getting called at.
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        for (int i = 0; i < powerUps.size(); i++) {
            PowerUp current = powerUps.get(i);
            current.update(container, game, delta);

            if (!current.isActive()) {
                if (current.getBounds().intersects(ship.getBounds())) {
                    current.fireEffect(ship);
                }
            }

            if (current.isDead()) {
                current.endEffect(ship);
                powerUps.remove(i--);
            }
        }

        if (((elapsed += delta) > nextPowerUpTime) &&
              powerUps.size() <= MAX_POWERUPS) {
            elapsed = 0;
            nextPowerUpTime = rand.nextInt(LOWEST_POWER_UP_TIME,
                    HIGHEST_POWER_UP_TIME);
            addRandomPowerUp();
        }
    }

    private void addPowerUp(PowerUp powerUp) {
        powerUps.add(powerUp);
    }

    private void addRandomPowerUp() {
        double num = rand.nextDouble();
        int randX = rand.nextInt(Asteroids.GAME_WIDTH);
        int randY = rand.nextInt(Asteroids.GAME_HEIGHT);
        if (num < .05) {
            addPowerUp(new SuperWeaponPwrUp(randX, randY));
        } else if (num < 0.15) {
            addPowerUp(new LifePowerUp(randX, randY));
        } else if (num < 0.4) {
            addPowerUp(new MissilePowerUp(randX, randY));
        } else {
            addPowerUp(new GatlingPowerUp(randX, randY));
        }
    }
}
