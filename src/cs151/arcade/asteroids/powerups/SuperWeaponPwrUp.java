package cs151.arcade.asteroids.powerups;

import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.weapons.guns.LaserRifle;
import cs151.arcade.asteroids.weapons.guns.SuperWeapon;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * This PowerUp gives the player a SuperWeapon for 5 seconds.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/15/14
 */
public class SuperWeaponPwrUp extends PowerUp {

    /**
     * Creates a SuperWeapon power up at the specified position
     *
     * @param x the starting x coordinate
     * @param y the starting y coordinate
     */
    public SuperWeaponPwrUp(float x, float y) {
        super(x, y, 5000);
        initImage();
        setWidth(getImage().getWidth());
        setHeight(getImage().getHeight());
    }

    /**
     * Called when the ship interacts with this power up
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    @Override
    public void startEffect(Ship ship) {
        ship.setWeapon(new SuperWeapon());
    }

    /**
     * Called when the power up has ran out
     *
     * @param ship the ship that is receiving the effect of this power up
     */
    @Override
    public void endEffect(Ship ship) {
        ship.setWeapon(new LaserRifle());
    }

    /**
     * Load the image for the object in this method.  Be sure to adjust
     * dimensions in here as well... (it is in this method because you can
     * change the dimensions based on the image's dimensions)
     */
    @Override
    protected void initImage() {
        try {
            setImage(new Image("res/powerups/Super.png").getScaledCopy(.65f));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
