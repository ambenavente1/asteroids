package cs151.arcade.asteroids.states;

import cs151.arcade.asteroids.collisions.CollisionManager;
import cs151.arcade.asteroids.io.HighScore;
import cs151.arcade.asteroids.io.HighScoreReader;
import cs151.arcade.asteroids.io.HighScoreWriter;
import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.objects.Ship;
import cs151.arcade.asteroids.objects.managers.AsteroidManager;
import cs151.arcade.asteroids.particles.ParticleSystem;
import cs151.arcade.asteroids.powerups.PowerUpManager;
import cs151.arcade.asteroids.ui.ControlManager;
import cs151.arcade.asteroids.ui.Label;
import cs151.arcade.asteroids.util.StarBackground;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.opengl.renderer.Renderer;
import org.newdawn.slick.opengl.renderer.SGL;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * GameState that handles the actual game play.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class GameplayState extends BasicGameState {

    /**
     * List of sub states
     */
    private enum SubState {
        NAME, PLAYING, GAMEOVER, HIGHSCORES
    }

    /**
     * Time that game over is displayed on the screen before
     */
    private static final int MAX_GAME_OVER_TIME = 3000;

    /**
     * Asteroid manager in charge of updating asteroids
     */
    private AsteroidManager asteroidManager;

    /**
     * Infinite background of a star field
     */
    private StarBackground starBackground;

    /**
     * Ship controlled by the player
     */
    private Ship ship;

    /**
     * Manager that checks up on collision between bullets, asteroids,
     * and player
     */
    private CollisionManager collisionManager;

    /**
     * Manages power ups collectable by the player
     */
    private PowerUpManager powerUpManager;

    /**
     * Sub state in the game
     */
    private SubState subState;

    /**
     * Amount of time the game has been in the game over state
     */
    private int gameOverTime;

    /**
     * Writes the high score to the high score file
     */
    private HighScoreWriter writer;

    /**
     * The name of the player
     */
    private String name;

    /**
     * Manages controls on the screen
     */
    private ControlManager highScoreLabels;

    /**
     * Text field to type in the player's name
     */
    private TextField textField;

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.GAMEPLAY.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        asteroidManager = new AsteroidManager();
        starBackground = new StarBackground(.4f);
        ship = new Ship();
        collisionManager = new CollisionManager(asteroidManager, ship);
        powerUpManager = new PowerUpManager(ship);
        subState = SubState.NAME;
        gameOverTime = 0;
        writer = new HighScoreWriter();
        highScoreLabels = new ControlManager();
        textField = new TextField(container, container.getGraphics()
                .getFont(), Asteroids.GAME_WIDTH / 2 - 140 / 2,
                Asteroids.GAME_HEIGHT / 2, 140, 35
        );
        textField.setMaxLength(20);
        textField.setBackgroundColor(Color.white);
        textField.setTextColor(Color.black);
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * render an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {

        starBackground.render(container, game, g);

        if (subState == SubState.NAME) {
            String namePrompt = "Enter your name: ";
            g.drawString(namePrompt,
                         textField.getX(),
                         textField.getY() - g.getFont().getLineHeight() - 10);
            textField.render(container, g);
        } else {
            asteroidManager.render(container, game, g);

            if (subState == SubState.PLAYING) {
                powerUpManager.render(container, game, g);
                ship.render(container, game, g);
            } else if (subState == SubState.GAMEOVER) {
                String msg = "G A M E  O V E R\n" +
                        "You scored " + ship.getPoints() + " points";
                g.drawString(msg,
                        Asteroids.GAME_WIDTH / 2 - g.getFont().getWidth(msg) /
                                2,
                        Asteroids.GAME_HEIGHT / 2 - g.getFont().getLineHeight()
                                / 2
                );
            } else if (subState == SubState.HIGHSCORES) {
                highScoreLabels.render(container, game, g);

                String msg = "Press [ENTER] to return to the menu...";
                g.drawString(msg,
                        Asteroids.GAME_WIDTH / 2 - g.getFont().getWidth
                                (msg) / 2,
                        Asteroids.GAME_HEIGHT - g.getFont().getLineHeight
                                () - 40
                );
            }

            g.drawString("Points: " + ship.getPoints(),
                    10,
                    Asteroids.GAME_HEIGHT
                            - g.getFont().getLineHeight() - 10
            );
            ParticleSystem.render(container, game, g);
        }
        TextureImpl.bindNone();
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        starBackground.update(container, game, delta);

        if (subState == SubState.NAME) {
            textField.setFocus(true);
            if (container.getInput().isKeyPressed(Input.KEY_ENTER)) {
                name = textField.getText();
                subState = SubState.PLAYING;
                textField.setFocus(false);
            }
        } else {
            ParticleSystem.update(container, game, delta);
            asteroidManager.update(container, game, delta);
            if (subState == SubState.PLAYING) {
                ship.update(container, game, delta);
                collisionManager.update(container, game, delta);
                powerUpManager.update(container, game, delta);

                if (asteroidManager.isEmpty()) {
                    ship.reset();
                    asteroidManager.levelUp();
                    asteroidManager.restart();
                }

                if (ship.isGameOver()) {
                    subState = SubState.GAMEOVER;
                    writeScore();
                }
            } else if (subState == SubState.GAMEOVER) {
                if ((gameOverTime += delta) >= MAX_GAME_OVER_TIME) {
                    subState = SubState.HIGHSCORES;
                    addHighScoreLabels(container.getGraphics());
                }
            } else if (subState == SubState.HIGHSCORES) {
                if (container.getInput().isKeyPressed(Input.KEY_ENTER)) {
                    game.enterState(States.MENU.getID(),
                            new FadeOutTransition(),
                            new FadeInTransition());
                }
            }

//            if (container.getInput().isMouseButtonDown(Input
//                    .MOUSE_LEFT_BUTTON)) {
//               ParticleSystem.addParticles(new Vector2f(container.getInput
//                 ().getMouseX(), container.getInput().getMouseY()),
//                        100, 2, 2, 1, Color.orange);
//                ParticleSystem.addExplosion(new Vector2f(container.getInput
//                      ().getMouseX(), container.getInput().getMouseY()));
//            }
        }
    }

    /**
     * Writes the player's high score using the HighScoreWriter
     */
    private void writeScore() {
        writer.writeScore(name, ship.getPoints());
    }

    /**
     * Adds all the high score labels to the ControlManager
     *
     * @param g graphics context to draw to
     */
    private void addHighScoreLabels(Graphics g) {
        List<HighScore> scores = new HighScoreReader().readScores();

        Label title = new Label("lblTitle", "H I G H  S C O R E S");
        title.setFont(g.getFont());
        title.setWidth(title.getFont().getWidth(title.getText()));
        title.setHeight(title.getFont().getLineHeight());
        title.setPos(new Vector2f(Asteroids.GAME_WIDTH / 2 -
                                  title.getWidth() / 2,
                                  90));
        highScoreLabels.add(title);

        int LABEL_PADDING = 15;

        int i = 1;
        Vector2f start = new Vector2f(0, title.getPos().y +
                LABEL_PADDING * 4);
        for (HighScore score : scores) {
            Label label = new Label("lblScore" + i,
                    i++ + ". " + score.formalString());
            label.setFont(g.getFont());
            label.setWidth(label.getFont().getWidth(label.getText()));
            label.setHeight(label.getFont().getLineHeight());
            start.x = Asteroids.GAME_WIDTH / 2 - label.getWidth() / 2;
            start.y += label.getHeight() + LABEL_PADDING;
            label.setPos(start.copy());
            highScoreLabels.add(label);
        }
    }

    /**
     * Called when the game enters this state
     *
     * @param container the container holding this game
     * @param game      the game in charge of changing states
     * @throws SlickException
     */
    @Override
    public void enter(GameContainer container, StateBasedGame game) throws
            SlickException {
        init(container, game);
    }
}
