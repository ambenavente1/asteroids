package cs151.arcade.asteroids.states;

import cs151.arcade.asteroids.io.HighScore;
import cs151.arcade.asteroids.io.HighScoreReader;
import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.ui.ControlManager;
import cs151.arcade.asteroids.ui.Label;
import cs151.arcade.asteroids.ui.LinkLabel;
import cs151.arcade.asteroids.ui.events.ActionArgs;
import cs151.arcade.asteroids.ui.events.ActionDoer;
import cs151.arcade.asteroids.util.StarBackground;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * The purpose of this game state is to display the scores from the score
 * file
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/29/2014
 */
public class HighScoreState extends BasicGameState {

    /**
     * Manages the controls on the screen (i.e. labels);
     */
    private ControlManager highScoreLabels;

    /**
     * The game in charge of switching game states
     */
    private StateBasedGame parent;

    /**
     * Infinite scrolling background
     */
    private StarBackground background;

    /**
     * Creates a new high score state passing in the parent game to switch
     * states
     *
     * @param parent the game in charge of switching states
     */
    public HighScoreState(StateBasedGame parent) {
        this.parent = parent;
    }

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.HIGH_SCORES.getID();
    }

    /**
     * Adds all the labels from the high score state
     *
     * @param g the graphic context to draw to
     */
    private void addHighScoreLabels(Graphics g) {
        List<HighScore> scores = new HighScoreReader().readScores();

        Label title = new Label("lblTitle", "H I G H  S C O R E S");
        title.setFont(g.getFont());
        title.setWidth(title.getFont().getWidth(title.getText()));
        title.setHeight(title.getFont().getLineHeight());
        title.setPos(new Vector2f(Asteroids.GAME_WIDTH / 2 -
                title.getWidth() / 2,
                90
        ));
        highScoreLabels.add(title);

        int LABEL_PADDING = 15;

        int i = 1;
        Vector2f start = new Vector2f(0, title.getPos().y +
                LABEL_PADDING * 4);
        for (HighScore score : scores) {
            Label label = new Label("lblScore" + i,
                    i++ + ". " + score.formalString());
            label.setFont(g.getFont());
            label.setWidth(label.getFont().getWidth(label.getText()));
            label.setHeight(label.getFont().getLineHeight());
            start.x = Asteroids.GAME_WIDTH / 2 - label.getWidth() / 2;
            start.y += label.getHeight() + LABEL_PADDING;
            label.setPos(start.copy());
            highScoreLabels.add(label);
        }
    }

    /**
     * Initializes this game state
     *
     * @param gameContainer the container holding the game
     * @param stateBasedGame the game in charge of switching game states
     * @throws SlickException
     */
    @Override
    public void init(GameContainer gameContainer, StateBasedGame stateBasedGame) throws SlickException {
        Graphics g = gameContainer.getGraphics();
        Font font = g.getFont();

        highScoreLabels = new ControlManager();
        addHighScoreLabels(gameContainer.getGraphics());

        LinkLabel lblGoBack = new LinkLabel("lblGoBack", "Back to Menu");
        lblGoBack.setFont(g.getFont());
        lblGoBack.setWidth(g.getFont().getWidth(lblGoBack.getText()));
        lblGoBack.setHeight(g.getFont().getLineHeight());
        lblGoBack.setPos(new Vector2f(10,
                Asteroids.GAME_HEIGHT - lblGoBack.getHeight() - 10));
        lblGoBack.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                parent.enterState(States.MENU.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        });
        highScoreLabels.add(lblGoBack);

        background = new StarBackground(.25f);
    }

    /**
     * Renders this state
     *
     * @param gameContainer  the container holding the game
     * @param stateBasedGame the game in charge of switching game states
     * @param graphics the graphics context to draw to
     * @throws SlickException
     */
    @Override
    public void render(GameContainer gameContainer, StateBasedGame stateBasedGame, Graphics graphics) throws SlickException {
        background.render(gameContainer, stateBasedGame, graphics);
        highScoreLabels.render(gameContainer, stateBasedGame, graphics);
    }

    /**
     * Updates the game state
     *
     * @param gameContainer the container holding the game
     * @param stateBasedGame the game in charge of switching game states
     * @param i the amount of time from the last update
     * @throws SlickException
     */
    @Override
    public void update(GameContainer gameContainer, StateBasedGame stateBasedGame, int i) throws SlickException {
        background.update(gameContainer, stateBasedGame, i);
        highScoreLabels.update(gameContainer, stateBasedGame, i);
    }

    /**
     * Called when the game enters this state
     *
     * @param container the container holding the game
     * @param game the game in charge of switching  game states
     * @throws SlickException
     */
    @Override
    public void enter(GameContainer container, StateBasedGame game) throws SlickException {
        init(container, game);
    }
}
