package cs151.arcade.asteroids.states;

import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.ui.Control;
import cs151.arcade.asteroids.ui.ControlManager;
import cs151.arcade.asteroids.ui.Label;
import cs151.arcade.asteroids.ui.LinkLabel;
import cs151.arcade.asteroids.ui.events.ActionArgs;
import cs151.arcade.asteroids.ui.events.ActionDoer;
import cs151.arcade.asteroids.util.StarBackground;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.awt.Font;

/**
 * The main menu of the game.  The player can choose to play,
 * view high scores, or exit from the game in this GameState.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class MainMenuState extends BasicGameState {

    /**
     * In charge of managing all the controls rendered onto the screen
     */
    private ControlManager controls;

    /**
     * The game in charge of switching states
     */
    private StateBasedGame parentGame;

    /**
     * Infinite scrolling star field in the background
     */
    private StarBackground starBackground;

    /**
     * Creates a new MainMenuState passing in the to be used for changing
     * states
     *
     * @param parentGame the game in charge of changing states
     */
    public MainMenuState(StateBasedGame parentGame) {
        this.parentGame = parentGame;
    }

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.MENU.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        TrueTypeFont titleFont = new TrueTypeFont(new Font("Myriad-Pro " +
                "Regular", Font.PLAIN, 48), true);
        TrueTypeFont labelFont = new TrueTypeFont(new Font("Myriad-Pro " +
                "Regular", Font.PLAIN, 20), true);
        final int LABEL_PADDING = 15;

        controls = new ControlManager();

        Control title = new Label("lblTitle", "Asteroids");
        title.setFont(titleFont);
        title.setWidth(titleFont.getWidth(title.getText()));
        title.setHeight(titleFont.getLineHeight());
        title.setPos(new Vector2f(Asteroids.GAME_WIDTH / 2
                - title.getWidth() / 2, 90));
        controls.add(title);

        Vector2f startPos = title.getPos().copy();
        startPos.y += title.getHeight() + LABEL_PADDING*4;

        LinkLabel lblPlay = new LinkLabel("lblPlay",
                "Play");
        lblPlay.setFont(labelFont);
        lblPlay.setWidth(labelFont.getWidth(lblPlay.getText()));
        lblPlay.setHeight(labelFont.getLineHeight());
        lblPlay.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                parentGame.enterState(States.GAMEPLAY.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        });
        startPos.x = Asteroids.GAME_WIDTH / 2
                - lblPlay.getWidth() / 2;
        lblPlay.setPos(startPos.copy());
        lblPlay.setHighlightColor(Color.blue);
        controls.add(lblPlay);

        startPos.y += lblPlay.getHeight() + LABEL_PADDING;

        LinkLabel lblHighScores = new LinkLabel("lblHighScores",
                "High Scores");
        lblHighScores.setFont(labelFont);
        lblHighScores.setWidth(labelFont.getWidth(lblHighScores.getText()));
        lblHighScores.setHeight(labelFont.getHeight());
        lblHighScores.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                parentGame.enterState(States.HIGH_SCORES.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        });
        startPos.x = Asteroids.GAME_WIDTH / 2 - lblHighScores.getWidth() / 2;
        lblHighScores.setPos(startPos.copy());
        lblHighScores.setHighlightColor(Color.blue);
        controls.add(lblHighScores);

        startPos.y += lblHighScores.getHeight() + LABEL_PADDING;

        LinkLabel lblQuit = new LinkLabel("lblQuit", "Quit");
        lblQuit.setFont(labelFont);
        lblQuit.setWidth(labelFont.getWidth(lblQuit.getText()));
        lblQuit.setHeight(labelFont.getHeight());
        lblQuit.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                parentGame.enterState(States.QUIT.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        });
        startPos.x = Asteroids.GAME_WIDTH / 2 - lblQuit.getWidth() / 2;
        lblQuit.setPos(startPos.copy());
        lblQuit.setHighlightColor(Color.blue);
        controls.add(lblQuit);

        starBackground = new StarBackground(.25f);
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * render an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        controls.render(container, game, g);
        starBackground.render(container, game ,g);

        String names = "Anthony B + Daniel P";
        g.drawString(names, Asteroids.GAME_WIDTH - g.getFont().getWidth
                        (names),
                Asteroids.GAME_HEIGHT - g.getFont().getLineHeight());
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time that's passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        controls.update(container, game, delta);
        starBackground.update(container, game, delta);
    }
}
