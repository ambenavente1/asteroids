package cs151.arcade.asteroids.states;

/**
 * List of possible GameStates in the game
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public enum States {

    SPLASH(0),
    MENU(1),
    GAMEPLAY(2),
    HIGH_SCORES(3),
    QUIT(4);

    /**
     * The ID for this game state used by the state manager to switch into
     * a state without passing in a new state object.
     */
    private final int ID;

    /**
     * Create a new States enumeration element
     *
     * @param ID The ID for this state
     */
    private States(int ID) {
        this.ID = ID;
    }

    /**
     * Gets the id for this state element
     *
     * @return the id for this state
     */
    public int getID() {
        return ID;
    }
}
