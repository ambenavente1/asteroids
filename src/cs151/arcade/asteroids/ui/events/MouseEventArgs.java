/*
 * Copyright (c) 2014 Anthony Benavente
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cs151.arcade.asteroids.ui.events;

import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;

/**
 * Events describing what the state of the mouse was at the time this
 * object was created.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 2/19/14
 */
public class MouseEventArgs implements ActionArgs {

    /**
     * Position of the mouse
     */
    private Vector2f pos;

    /**
     * If the mouse's left button was clicked
     */
    private boolean leftClick;

    /**
     * If the mouse's right button was clicked
     */
    private boolean rightClick;

    /**
     * If the mouse's middle button was clicked
     */
    private boolean middleClick;

    /**
     * Creates a new MouseEventArgs object passing in the GameContainer's
     * input in order to gather information about the mouse
     *
     * @param input the GameContainer's input in charge of updating user
     *              interaction in the game
     */
    public MouseEventArgs(Input input) {
        pos = new Vector2f(input.getMouseX(), input.getMouseY());
        leftClick = input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON);
        rightClick = input.isMouseButtonDown(Input.MOUSE_RIGHT_BUTTON);
        middleClick = input.isMouseButtonDown(Input.MOUSE_MIDDLE_BUTTON);
    }

    /**
     * Gets the position of the mouse
     *
     * @return the position of the mouse
     */
    public Vector2f getPos() {
        return pos;
    }

    /**
     * Gets if the mouse's left button was clicked
     *
     * @return if the mouse's left button was clicked
     */
    public boolean isLeftClick() {
        return leftClick;
    }

    /**
     * Gets if the mouse's right button was clicked
     *
     * @return if the mouse's right button was clicked
     */
    public boolean isRightClick() {
        return rightClick;
    }

    /**
     * Gets if the mouse's middle button was clicked
     *
     * @return if the mouse's middle button was clicked
     */
    public boolean isMiddleClick() {
        return middleClick;
    }
}
