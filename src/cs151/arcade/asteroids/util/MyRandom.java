package cs151.arcade.asteroids.util;

import java.util.Random;

/**
 * Custom random number generator capable of getting random numbers within
 * a range.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public class MyRandom extends Random {

    /**
     * Gets a random integer within a range of [min, max) non-inclusive
     *
     * @param min the smallest number possible
     * @param max the largest number possible not including this one
     * @return a random number within the specified range
     */
    public int nextInt(int min, int max) {
        return nextInt(max - min) + min;
    }

    /**
     * Gets a random float within a range of [min, max) non-inclusive
     *
     * @param min the smallest number possible
     * @param max the largest number possible not including this one
     * @return a random number within the specified range
     */
    public float nextFloat(float min, float max) {
        return (nextFloat() * max) + min;
    }

}
