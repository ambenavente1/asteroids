package cs151.arcade.asteroids.util;

import cs151.arcade.asteroids.main.Asteroids;
import cs151.arcade.asteroids.objects.intefaces.Renderable;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Point;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents an endless star field that is drawn in the background of the
 * game.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public class StarBackground implements Updatable, Renderable {

    /**
     * Represents a single star
     */
    class Star {
        /**
         * The position of the star
         */
        Point pos;

        /**
         * The radius of the star
         */
        int radius;
    }

    /**
     * The list containing all the stars in the star field
     */
    private List<Star> stars;

    /**
     * The speed that the stars are moving downward
     */
    private float speed;

    /**
     * The random number generator for use in random stuff in this class
     */
    private MyRandom rand;

    /**
     * Creates a new StarBackground with a specified speed
     *
     * @param speed the speed stars are moving downward
     */
    public StarBackground(float speed) {
        this.speed = speed;
        this.stars = new ArrayList<Star>();
        this.rand = new MyRandom();

        for (int i = 0; i < 20; i++) {
            Star newStar = new Star();
            newStar.pos = new Point(rand.nextInt(Asteroids.GAME_WIDTH),
                                    rand.nextInt(Asteroids.GAME_HEIGHT));
            newStar.radius = rand.nextInt(2, 7);
            stars.add(newStar);
        }
    }

    /**
     * Renders the star field
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        for (Star p : stars) {
            g.setAntiAlias(true);
            Color oldColor = g.getColor();
            g.setColor(Color.white);
            g.fillOval(p.pos.getX(), p.pos.getY(), p.radius * 2, p.radius * 2);
            g.setColor(oldColor);
            g.setAntiAlias(false);
        }
    }

    /**
     * Updates the star field
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        for (Star p : stars) {
            p.pos.setY(p.pos.getY() + speed);
            if (p.pos.getY() > Asteroids.GAME_HEIGHT) {
                p.pos.setY(-40);
                p.pos.setX(rand.nextInt(Asteroids.GAME_WIDTH));
                p.radius = rand.nextInt(2, 7);
            }
        }
    }
}
