package cs151.arcade.asteroids.weapons;

import cs151.arcade.asteroids.objects.intefaces.Renderable;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.weapons.bullets.Bullet;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class representing a weapon which a player can fire bullets
 * toward asteroids or other ships to destroy them.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public abstract class Weapon implements Updatable, Renderable {

    /**
     * Amount of damage this weapon does
     */
    private float damage;

    /**
     * If the key for firing has been pressed, used for weapons that don't
     * have automatic fire
     */
    protected boolean keyDown;

    /**
     * The amount of time elapsed from the last shot (limited to 1
     * millisecond)
     */
    protected int coolDown;

    /**
     * The amount of time between shots that this weapon can fire (limited
     * to 1 millisecond).
     */
    protected final int maxCoolDown;

    /**
     * List containing all the bullets
     */
    private List<Bullet> bullets;

    /**
     * Creates a new weapon with a specified damage and cool down rate
     *
     * @param damage amount of damage this weapon does
     * @param maxCoolDown the amount of time between shots (-1 if not
     *                    automatic shooting)
     */
    public Weapon(float damage, int maxCoolDown) {
        this.damage         = damage;
        this.maxCoolDown    = maxCoolDown;
        this.coolDown       = 0;
        this.bullets        = new ArrayList<Bullet>();
    }

    /**
     * Updates the bullets and weapon
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        if (maxCoolDown >= 0) {
            coolDown -= delta;
        } else {
            if (keyDown) {
                keyDown = container.getInput().isKeyDown(Input.KEY_SPACE);
            }
        }

        for (int i = 0; i < bullets.size(); i++) {
            Bullet current = bullets.get(i);
            if (!current.isDead()) {
                current.update(container, game, delta);
            } else {
                bullets.remove(current);
            }
        }
    }

    /**
     * Renders the bullets of the weapon
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        for (Bullet b : bullets) {
            b.render(container, game, g);
        }
    }

    /**
     * Called when the gun fires
     *
     * @param e the events representing player information
     */
    public abstract void onFire(PlayerEventArgs e);

    /**
     * Adds a bullet to the gun
     *
     * @param bullet the bullet to add
     */
    protected void addBullet(Bullet bullet) {
        bullets.add(bullet);
    }

    /**
     * Gets the amount of damage this gun causes
     *
     * @return the amount of damage by this gun
     */
    public float getDamage() {
        return damage;
    }

    /**
     * Gets the list of bullets fired by this gun that are still alive
     *
     * @return the list of bullets for this weapon
     */
    public List<Bullet> getBullets() {
        return bullets;
    }

    /**
     * Removes all the bullets from the field
     */
    public void clear() {
        bullets.clear();
    }
}
