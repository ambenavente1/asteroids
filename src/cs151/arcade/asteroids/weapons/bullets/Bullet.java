package cs151.arcade.asteroids.weapons.bullets;

import cs151.arcade.asteroids.collisions.Collidable;
import cs151.arcade.asteroids.collisions.CollideEventArgs;
import cs151.arcade.asteroids.objects.StaticObject;
import cs151.arcade.asteroids.objects.intefaces.Renderable;
import cs151.arcade.asteroids.objects.intefaces.Updatable;
import cs151.arcade.asteroids.weapons.Weapon;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Abstract class representing a projectile launched from a weapon.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public abstract class Bullet extends StaticObject implements Updatable,
        Collidable{

    /**
     * The default speed of bullets when launched from guns
     */
    protected static final float SPEED = 13.5f;

    /**
     * The amount of time that the bullet is alive on the field (milliseconds)
     */
    private static final int MAX_LIVE = 800;

    /**
     * The amount of time that this bullet has been alive
     */
    private int liveTime;

    /**
     * If this bullet is dead or not
     */
    private boolean dead;

    /**
     * The weapon that "owns" this bullet
     */
    private Weapon parent;

    /**
     * Creates a bullet
     *
     * @param start the position
     * @param angle the angle of direction and facing
     * @param parent the weapon owning this bullet
     */
    public Bullet(Vector2f start, float angle, Weapon parent) {
        super(start.x, start.y);
        setFacingAngle(angle);
        setMovementAngle(angle);

        this.liveTime = 0;
        this.dead = false;
        this.parent = parent;
    }

    /**
     * Initializes the bullet's image
     */
    @Override
    protected abstract void initImage();

    /**
     * Renders the bullet
     *
     * @param container The container holding the game
     * @param game      The game itself (has the ability to change states)
     * @param g         The graphics object used to render images
     */
    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) {
        if (!dead) {
            super.render(container, game, g);
        }
    }

    /**
     * Updates the bullet
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        float xVel = (float) (SPEED * Math.cos(Math.toRadians(
                getMovementAngle()
        )));
        float yVel = (float) (SPEED * Math.sin(Math.toRadians(
                getMovementAngle()
        )));
        setVel(new Vector2f(xVel, yVel));
        applyVelocity();

        dead = (liveTime += delta) >= MAX_LIVE;
    }

    /**
     * Called when the bullet collides with something
     *
     * @param e arguments for another object colliding with this one
     */
    @Override
    public abstract void onCollide(CollideEventArgs e);

    /**
     * Gets if the bullet is dead i.e. it's live time surpassed the alotted
     * time
     *
     * @return if the bullet is dead
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Sets if this bullet is dead
     *
     * @param dead if this bullet is dead
     */
    protected void setDead(boolean dead) {
        this.dead = dead;
    }

    /**
     * Gets the amount of damage that the gun of this bullet does
     *
     * @return damage value of the gun
     */
    public float getParentDamage() {
        return parent.getDamage();
    }
}
