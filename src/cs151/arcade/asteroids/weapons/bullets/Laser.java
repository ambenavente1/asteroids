package cs151.arcade.asteroids.weapons.bullets;

import cs151.arcade.asteroids.collisions.CollideEventArgs;
import cs151.arcade.asteroids.weapons.Weapon;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * Represents the projectile shot from a laser gun
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public class Laser extends Bullet {

    /**
     * The image for the laser
     */
    private static Image GREEN_LASER;

    /**
     * The image for the laser when it has made contact with something
     */
    private static Image GREEN_LASER_HIT;
    static {
        try {
            GREEN_LASER = new Image("res/images/laserGreen.png");
            GREEN_LASER_HIT = new Image("res/images/laserGreenShot.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new laser
     *
     * @param start  the position that the laser starts at
     * @param angle  the angle the laser is facing and moving
     * @param parent the weapon that shot this laser
     */
    public Laser(Vector2f start, float angle, Weapon parent) {
        super(start, angle, parent);
        initImage();
        if (getImage() != null) {
            setWidth(getImage().getWidth());
            setHeight(getImage().getHeight());
            getImage().setRotation(getFacingAngle());
        } else {
            setWidth(33);
            setHeight(9);
        }
    }

    /**
     * Initializes the laser's image
     */
    @Override
    protected void initImage() {
        setImage(GREEN_LASER.copy());
    }

    /**
     * Called when the laser collides with something
     *
     * @param e arguments for another object colliding with this one
     */
    @Override
    public void onCollide(CollideEventArgs e) {
        setImage(GREEN_LASER_HIT.copy());
        setVel(new Vector2f());
        setDead(true);
    }
}
