package cs151.arcade.asteroids.weapons.bullets;

import cs151.arcade.asteroids.collisions.CollideEventArgs;
import cs151.arcade.asteroids.particles.ParticleSystem;
import cs151.arcade.asteroids.weapons.Weapon;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

import org.newdawn.slick.Image;

/**
 * Represents a projectile shot from a missile launcher
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/26/14
 */
public class Missile extends Bullet {

    /**
     * The image for the missile
     */
    private static Image MISSILE;
    static {
        try {
            MISSILE = new Image("res/images/missile.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new missile
     *
     * @param start  the start position of the bullet
     * @param angle  the starting angle of movement for the bullet
     * @param parent the weapon that shot this bullet
     */
    public Missile(Vector2f start, float angle, Weapon parent) {
        super(start, angle, parent);

        initImage();
        setWidth(getImage().getWidth());
        setHeight(getImage().getHeight());
        getImage().setRotation(angle);
    }

    /**
     * Initializes the missile's image
     */
    @Override
    protected void initImage() {
        setImage(MISSILE.copy());
    }

    /**
     * Called when the missile collides with something
     *
     * @param e arguments for another object colliding with this one
     */
    @Override
    public void onCollide(CollideEventArgs e) {
        // TODO: Explosion animation
        ParticleSystem.addExplosion(centerPos().copy());
        ParticleSystem.addExplosion(centerPos().copy());
        setDead(true);
    }
}
