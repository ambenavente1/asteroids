package cs151.arcade.asteroids.weapons.bullets;

import cs151.arcade.asteroids.collisions.CollideEventArgs;
import cs151.arcade.asteroids.weapons.Weapon;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

/**
 * Represents bullets shot from a plasma gun.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/29/2014
 */
public class Plasma extends Bullet {

    /**
     * The image for the plasma
     */
    static Image PLASMA;
    static {
        try {
            PLASMA = new Image("res/images/plasma.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new plasma bullet
     *
     * @param start  the start position of the bullet
     * @param angle  the starting angle of movement for the bullet
     * @param parent the weapon that shot this bullet
     */
    public Plasma(Vector2f start, float angle, Weapon parent) {
        super(start, angle, parent);
        initImage();
        if (getImage() != null) {
            setWidth(getImage().getWidth());
            setHeight(getImage().getHeight());
            getImage().setRotation(angle);
        } else {
            setWidth(8);
            setHeight(8);
        }
    }

    /**
     * Initializes the plasma's image
     */
    @Override
    protected void initImage() {
        setImage(PLASMA.copy());
    }

    /**
     * Called when the plasma collides with something
     *
     * @param e arguments for another object colliding with this one
     */
    @Override
    public void onCollide(CollideEventArgs e) {
        setDead(true);
    }
}
