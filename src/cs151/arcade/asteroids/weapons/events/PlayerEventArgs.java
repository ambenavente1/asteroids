package cs151.arcade.asteroids.weapons.events;

import cs151.arcade.asteroids.objects.Ship;
import org.newdawn.slick.geom.Vector2f;

/**
 * This is a wrapper class containing information about the player.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public class PlayerEventArgs {

    /**
     * Position of the player
     */
    private Vector2f pos;

    /**
     * Angle that the player is facing
     */
    private float facingAngle;

    /**
     * Angle that the player is moving
     */
    private float movingAngle;

    /**
     * Creates a new PlayerEventArgs object passing in the player in order
     * for the object to gather information
     *
     * @param player the player to gather information for
     */
    public PlayerEventArgs(Ship player) {
        this.pos = player.getPos().copy();
        this.facingAngle = player.getFacingAngle();
        this.movingAngle = player.getMovementAngle();
    }

    /**
     * Gets the position of the player described by this class
     *
     * @return the position of the player
     */
    public Vector2f getPos() {
        return pos;
    }

    /**
     * Gets the facing angle of the player at the time that this object was
     * created
     *
     * @return the facing angle of the player
     */
    public float getFacingAngle() {
        return facingAngle;
    }

    /**
     * Gets the moving angle of the player at the time that this object was
     * created
     *
     * @return the moving angle of the player
     */
    public float getMovingAngle() {
        return movingAngle;
    }
}
