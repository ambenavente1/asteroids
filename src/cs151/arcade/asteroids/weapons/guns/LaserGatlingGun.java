package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.bullets.Laser;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapon shoots laser bullets rapid-fire style.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/30/2014
 */
public class LaserGatlingGun extends Weapon {

    /**
     * Creates a new LaserGatlingGun
     */
    public LaserGatlingGun() {
        super(80, 80);
    }

    /**
     * Called when the gun is fired
     *
     * @param e the events representing player information
     */
    @Override
    public void onFire(PlayerEventArgs e) {
        if (coolDown <= 0) {
            coolDown = maxCoolDown;
            addBullet(new Laser(e.getPos().copy(), e.getFacingAngle(), this));
        }
    }
}
