package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.bullets.Laser;
import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapon fires laser bullets at a semi-auto rate.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/24/14
 */
public class LaserRifle extends Weapon {

    public LaserRifle() {
        super(300, -1);
    }

    @Override
    public void onFire(PlayerEventArgs e) {
        if (!keyDown) {
            keyDown = true;
            addBullet(new Laser(e.getPos().copy(), e.getFacingAngle(), this));
        }
    }
}
