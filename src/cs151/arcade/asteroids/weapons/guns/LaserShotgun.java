package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.bullets.Laser;
import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapon shoots 3 laser bullets in an angled fashion.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/26/14
 */
public class LaserShotgun extends Weapon {

    /**
     * Angle between the three shots
     */
    private static final float ANGLE = 18f;

    /**
     * Creates a new LaserShotgun object
     */
    public LaserShotgun() {
        super(300, -1);
    }

    /**
     * Called when the gun is fired
     *
     * @param e the events representing player information
     */
    @Override
    public void onFire(PlayerEventArgs e) {
        if (!keyDown) {
            keyDown = true;
            // Adds three laser shots when the shoot button is pressed
            addBullet(new Laser(e.getPos().copy(),
                    e.getFacingAngle(),
                    this));
            addBullet(new Laser(e.getPos().copy(),
                    e.getFacingAngle() - ANGLE,
                    this));
            addBullet(new Laser(e.getPos().copy(),
                    e.getFacingAngle() + ANGLE,
                    this));
        }
    }
}
