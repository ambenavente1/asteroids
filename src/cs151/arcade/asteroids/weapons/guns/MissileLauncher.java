package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.bullets.Missile;
import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapon launches missiles and explodes stuff.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/26/14
 */
public class MissileLauncher extends Weapon{

    /**
     * Creates a new MissileLauncher
     */
    public MissileLauncher() {
        super(1500, -1);
    }

    /**
     * Called when the gun is fired
     *
     * @param e the events representing player information
     */
    @Override
    public void onFire(PlayerEventArgs e) {
        if (!keyDown) {
            keyDown = true;
            addBullet(new Missile(e.getPos().copy(), e.getFacingAngle(),
                    this));
        }
    }
}
