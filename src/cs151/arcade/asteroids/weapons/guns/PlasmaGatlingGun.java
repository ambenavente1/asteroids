package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.bullets.Plasma;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapon shoots plasma bullets rapid-fire style.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/29/2014
 */
public class PlasmaGatlingGun extends Weapon {

    /**
     * Creates a new plasma gatling gun
     */
    public PlasmaGatlingGun() {
        super(80, 40);
    }

    /**
     * Called when the gun is fired
     *
     * @param e the events representing player information
     */
    @Override
    public void onFire(PlayerEventArgs e) {
        if (coolDown <= 0) {
            coolDown = maxCoolDown;
            addBullet(new Plasma(e.getPos().copy(), e.getFacingAngle(),
                    this));
        }
    }
}
