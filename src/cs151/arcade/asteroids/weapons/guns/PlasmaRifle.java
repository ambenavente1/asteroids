package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.bullets.Plasma;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapon fires plasma bullets at a semi-auto rate.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/30/2014
 */
public class PlasmaRifle extends Weapon{

    /**
     * Creates a new PlasmaRifle object
     */
    public PlasmaRifle() {
        super(550, -1);
    }

    /**
     * Called when the gun is fired
     *
     * @param e the events representing player information
     */
    @Override
    public void onFire(PlayerEventArgs e) {
        if (!keyDown) {
            keyDown = true;
            addBullet(new Plasma(e.getPos().copy(), e.getFacingAngle(),
                    this));
        }
    }
}
