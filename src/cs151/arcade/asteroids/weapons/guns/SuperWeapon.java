package cs151.arcade.asteroids.weapons.guns;

import cs151.arcade.asteroids.weapons.Weapon;
import cs151.arcade.asteroids.weapons.bullets.Laser;
import cs151.arcade.asteroids.weapons.bullets.Plasma;
import cs151.arcade.asteroids.weapons.events.PlayerEventArgs;

/**
 * This weapons shoots a circle of bullets around the player at 18 degree
 * increments.  A lot of damage gets done by this weapon,
 * hence it being known as the SuperWeapon.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/30/2014
 */
public class SuperWeapon extends Weapon {

    /**
     * Angle between bullets being launched from the player
     */
    private static final int INCREMENT = 18;

    /**
     * Creates a SuperWeapon object
     */
    public SuperWeapon() {
        super(200, 150);
    }

    /**
     * Called when the gun is fired
     *
     * @param e the events representing player information
     */
    @Override
    public void onFire(PlayerEventArgs e) {
        if (coolDown <= 0) {
            coolDown = maxCoolDown;

            for (int i = 0; i < 360; i += INCREMENT) {
                addBullet(new Laser(e.getPos().copy(),
                                    e.getFacingAngle() + i,
                                    this));
            }

            for (int i = INCREMENT / 2; i < 360; i += INCREMENT) {
                addBullet(new Plasma(e.getPos().copy(),
                                     e.getFacingAngle() + i,
                                     this));
            }
        }
    }
}
